sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device"
], function (JSONModel, Device) {
	"use strict";

	return {

		createDeviceModel: function () {
			var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},
		createInputModel: function () {
			var oInputModel = new JSONModel({
				//id: 0,
				OTP: "",
				fName: "",
				lName: "",
				email: "",
				designation: "",
				country: "",
				mobileNum: "",
				company: ""
			});
			return oInputModel;
		}

	};
});