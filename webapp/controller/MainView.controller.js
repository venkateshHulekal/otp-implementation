sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageBox",
	'sap/m/Dialog',
	'sap/m/Button',
	'jquery.sap.global',
	'sap/m/MessageToast'
], function (Controller, MessageBox, Dialog, Button, jquery, MessageToast) {
	"use strict";

	return Controller.extend("com.capgemini.coe.ChatBotDetailPicker.controller.MainView", {
		onInit: function () {
			this.inputModel = this.getOwnerComponent().getModel("inputModel");
		},

		onGeneratePress: function () {
			if (!this._validateSubmit()) {
				return;
			}
			var that = this;
			this.OTP = Math.floor(1000 + Math.random() * 9000);
			that.inputModel.setProperty("/OTP", this.OTP)
			var oInputData = this.inputModel.getData();
			that.pushData(oInputData);
			//that.openWaitFunction();
		},

		_validateSubmit: function () {
			var bValid = true;
			if (this.inputModel.getProperty("/fName") === "" || this.inputModel.getProperty("/lName") === "") {
				MessageBox.error("Please enter your FullName.");
				return false;
			}
			if (this.inputModel.getProperty("/email") === "") {
				MessageBox.error("Please enter your email ID.");
				return false;
			}
			if (this.inputModel.getProperty("/designation") === "") {
				MessageBox.error("Designation field can't be empty.");
				return false;
			}
			if (this.inputModel.getProperty("/country") === "" || this.inputModel.getProperty("/mobileNum") === "") {
				MessageBox.error("Please enter Correct Phone Number with country code.");
				return false;
			}
			if (this.inputModel.getProperty("/company") === "") {
				MessageBox.error("Please enter your company Name.");
				return false;
			}

			if (!bValid)
				sap.m.MessageBox.error("Please enter all mandatory fields.");

			return bValid;
		},

		validateEmail: function (oEvent) {
			var email = this.inputModel.getProperty("/email");

			if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
				return (true);
			}
			MessageBox.error(email + " is not a valid email address");
			return (false);
		},

		pushData: function (data) {
			var oDataModel = this.getOwnerComponent().getModel("OTPmodel");
			oDataModel.create("/CHATBOTUSERS", data, {
				success: function (oData, oResponse) {
					this.sendEmail();
					this.openWaitFunction();
				}.bind(this),
				error: function (error) {
					MessageBox.error("Oops! Something went wrong" + error.responseText);
				}
			});
		},

		sendEmail: function () {
			this.getTokenForWOrkflow();

			var payload = JSON.stringify({
				"definitionId": "otpservice",
				"context": {
					"emailId": this.inputModel.getProperty("/email"),
					"OTP": this.inputModel.getProperty("/OTP")
				}
			});

			var xhr = new XMLHttpRequest();
			xhr.withCredentials = false;

			xhr.addEventListener("readystatechange", function () {
				if (this.readyState === this.DONE) {
					console.log(this.responseText);
				}
			});

			var url1 = '/destination/bpmworkflowruntime1/workflow-service/rest/v1/workflow-instances';
			//setting request method
			xhr.open("POST", url1);

			//adding request headers
			xhr.setRequestHeader("X-CSRF-Token", this._token);
			xhr.setRequestHeader("Content-Type", "application/json");
			xhr.setRequestHeader("Accept", "application/json");

			//sending request
			xhr.send(payload);
		},

		getTokenForWOrkflow: function () {
			var that = this;
			var url = '/destination/bpmworkflowruntime1/workflow-service/rest/v1/xsrf-token';
			$.ajax({
				url: url,
				method: "GET",
				async: false,
				headers: {
					"X-CSRF-Token": "Fetch",
					"Content-Type": "application/json"
				},
				success: function (result, xhr, data) {
					that._token = data.getResponseHeader("X-CSRF-Token");
				}
			});
		},

		openWaitFunction: function () {
			if (!this._oPopover) {
				this._oPopover = sap.ui.xmlfragment("idWaitingForm", "com.capgemini.coe.ChatBotDetailPicker.view.waiting", this);
				this.getView().addDependent(this._oPopover);
			}

			// open dialog
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oPopover);
			this._oPopover.open();

			// simulate end of operation
			jQuery.sap.delayedCall(240000, this, function () {
				this.deleteEntry();
				this._oPopover.close();
			});

		},

		deleteEntry: function () {
			var oDataModel = this.getOwnerComponent().getModel("OTPmodel");
			oDataModel.remove("/CHATBOTUSERS(" + this.OTP + ")", {
				success: function (oData, oResponse) {
					this.clearModel();
				}.bind(this),
				error: function (error) {
					///MessageBox.error("Oops! Something went wrong" + error.responseText);
				}
			});
		},

		clearModel: function () {
			this.inputModel.setData("");
		},

		onValidatePress: function () {
			var inputOTP = sap.ui.core.Fragment.byId("idWaitingForm", "idOTP").getValue();
			var oDataModel = this.getOwnerComponent().getModel("OTPmodel");
			oDataModel.read("/CHATBOTUSERS(" + inputOTP + ")", {
				success: function (oData, oResponse) {
					MessageToast.show("You are now Authenticated.");
					this.deleteEntry();
					this._oPopover.close();
				}.bind(this),
				error: function (error) {
					MessageToast.show("You have entered a wrong OTP! Please retry with valid OTP.");
				}
			});
		}

	});
});